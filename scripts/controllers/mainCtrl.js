(function () {

    var mainCtrl = function ($scope, crud_service, $timeout, $modal, $location, $rootScope) {


        //find city or cities and show them including they places on map
        $scope.find = function () {
            crud_service.get_item('cities?query='+$scope.search_query).then(function (response) {
                $scope.cities = response.data;
                $scope.map = [];
                $timeout(function () {
                    for(var i=0; i<$scope.cities.length; i++) {
                        var mapProp = {
                            center: new google.maps.LatLng($scope.cities[i].coordinates_x, $scope.cities[i].coordinates_y),
                            zoom: 5,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };
                        $scope.map[i] = new google.maps.Map(document.getElementById("googleMap_"+$scope.cities[i].name), mapProp);
                        var marker = new google.maps.Marker({
                            map: $scope.map[i],
                            position: new google.maps.LatLng($scope.cities[i].coordinates_x, $scope.cities[i].coordinates_y),
                            title: 'my title'
                        });
                    }
                });
            }, function (error) { console.log('error',error.data); })
        };

        //you can create new city from any page, so this function is on mainCtrl
        $scope.new = function () {
            var modalInstance = $modal.open({
                templateUrl: 'views/newCity.html',
                size: 'modal-lg',
                resolve: {
                    city: function(){
                        return null;
                    }
                },
                controller: 'modalCtrl'
            });

            modalInstance.result.then(function (response) {
                console.log('modal response edis',response);
                $location.path('/home/city/'+response.data.id);
            }, function (error) {})
        };

        $scope.login = function () {
            var data = {"access": $scope.user};
            crud_service.post_item('access', data).then(function (response) {
                console.log(response);
                window.localStorage.setItem('token',response.data.token);
                window.localStorage.setItem('userId',response.data.user_id);
                $location.path('/home/main');
            }, function (error) {

            })
        };

        $scope.logout = function(){
            var promise = crud_service.delete_item('access/'+window.localStorage.userId);
            promise.then(function(response){
                window.localStorage.clear();
                $rootScope.logged = false;
                $location.path('/home/main');
            },function(error){});
        };

    };

    APP.controller('mainCtrl',mainCtrl);
}());