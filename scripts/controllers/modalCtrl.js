(function () {

    var modalCtrl = function ($scope, $modalInstance, crud_service, city) {
        $scope.edit = false;
        $scope.tags = [];
        if(city){
            $scope.city = city;
            for(var i=0; i< $scope.city.tags.length; i++) {
                $scope.tags.push($scope.city.tags[i].name);
            }
            $scope.edit = true;
        }

        $scope.update = function () {
            var data = { "city" : {
                "name": $scope.city.name,
                "coordinates_x": $scope.city.coordinates_x,
                "coordinates_y": $scope.city.coordinates_y,
                "all_tags": $scope.tags
            } };
            crud_service.put_item('cities/'+$scope.city.id, data).then(function (response) {
                $modalInstance.close(response);
            }, function (error) {
                $modalInstance.dismiss();
            })
        };

        $scope.create = function () {

            var data = { "city" : {
                "name": $scope.city.name,
                "coordinates_x": $scope.city.coordinates_x,
                "coordinates_y": $scope.city.coordinates_y,
                "all_tags": $scope.tags
            } };
            crud_service.post_item('cities', data).then(function (response) {
                $modalInstance.close(response);
            }, function (error) {
                $modalInstance.dismiss();
            })
        }

    };

    APP.controller('modalCtrl', modalCtrl);
}());