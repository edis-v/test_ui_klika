(function () {

    var citiesCtrl = function ($scope, cities) {
        $scope.cities = [];
        $scope.cities = cities.data;
    };

    APP.controller('citiesCtrl', citiesCtrl);
}());