(function () {

    var cityCtrl = function ($scope, city, $modal, crud_service, $timeout, $location) {

        $scope.tags = [];
        if(city) {
            $scope.city = city.data;
            console.log('scope.city',$scope.city.tags);
            for(var i=0; i< $scope.city.tags.length; i++) {
                $scope.tags.push($scope.city.tags[i].name);
            }

            $timeout(function () {
                var mapProp = {
                    center: new google.maps.LatLng($scope.city.coordinates_x, $scope.city.coordinates_y),
                    zoom: 5,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                $scope.map = new google.maps.Map(document.getElementById("googleMap_"+$scope.city.name), mapProp);
                var marker = new google.maps.Marker({
                    map: $scope.map,
                    position: new google.maps.LatLng($scope.city.coordinates_x, $scope.city.coordinates_y),
                    title: 'my title'
                });
            });
        }

        $scope.edit = function (city) {
            var modalInstance = $modal.open({
                templateUrl: 'views/editCity.html',
                size: 'modal-lg',
                resolve: {
                    city: function(){
                        return city;
                    }
                },
                controller: 'modalCtrl'
            });

            modalInstance.result.then(function (response) {
               $scope.city = response.data;
            }, function (error) {

            })
        };


        $scope.delete = function (city) {
            var r = confirm("Are You Sure?");
            if(r == true) {
                crud_service.delete_item('cities/'+city.id).then(function (response) {
                    $location.path('/home/cities');
                }, function (error) {

                });
            } else console.log('else');
        };

    };

    APP.controller('cityCtrl', cityCtrl);
}());