var APP = angular.module('klika-ui',['ui.router', 'ui.bootstrap']);

APP.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {

    $urlRouterProvider.otherwise('/home/main');

    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: 'views/layout.html',
            controller: 'mainCtrl'
        })
        .state('home.main', {
            url: '/main',
            templateUrl: 'views/main.html',
            controller: 'mainCtrl'
        })
        .state('login', {
            url: '/login',
            templateUrl: 'views/login.html',
            controller: 'mainCtrl'
        })
        .state('home.cities', {
            url: '/cities',
            templateUrl: 'views/cities.html',
            resolve: {
                cities: function (crud_service) {
                    return crud_service.get_item('cities');
                }
            },
            controller: 'citiesCtrl'
        })
        .state('home.city', {
            url: '/city/:id',
            templateUrl: 'views/city.html',
            resolve: {
                city: function (crud_service, $stateParams) {
                    return crud_service.get_item('cities/'+ $stateParams.id);
                }
            },
            controller: 'cityCtrl'
        });

    $httpProvider.interceptors.push('intercepter');
});

APP.run(["$rootScope", function($rootScope, $location){
    $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams){
            $rootScope.logged = false;
            if (window.localStorage.getItem('token')!= undefined) {
                $rootScope.logged = true;
            }
        });
}]);